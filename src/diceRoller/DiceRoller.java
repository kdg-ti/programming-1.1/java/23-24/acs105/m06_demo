package diceRoller;

import java.util.Arrays;

public class DiceRoller {
	public static void main(String[] args) {
		Dice eightSided = new Dice(8);
		System.out.println(eightSided);
		System.out.println("statistics for "
			+ eightSided
			+ ":"
			+ Arrays.toString(getStatistics(eightSided))
		);
		Dice sixSided = new Dice(6);
		System.out.println("statistics for "
			+ sixSided
			+ ":"
			+ Arrays.toString(getStatistics(sixSided))
		);

		// an array of dices
		Dice[] diceCollection = {eightSided, sixSided, new Dice(20), new Dice(12)};
	}

	private static int[] getStatistics(Dice dice) {
		int[] statistics = new int[dice.getSides()];
		for(int rolls = 0; rolls <= 6000;rolls++){
			// shorter form with increment notation
			//statistics[dice.roll()-1]++;
			statistics[dice.roll()-1]=statistics[dice.roll()-1] +1;
		}
		return statistics;
	}
}
