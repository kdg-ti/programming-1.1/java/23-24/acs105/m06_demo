package diceRoller;

import java.util.Random;

public class Dice {
	private int sides;
	private Random random = new Random();

	public Dice(int sides) {
		this.sides = sides;
	}

	public int getSides() {
		return sides;
	}

	public int roll(){
		// both uses of random give the same result
		//return random.nextInt(1,sides+1);
		return random.nextInt(sides)+1;
	}

	@Override
	public String toString() {
		return String.format("dice with %d sides",sides);
	}
}
