import java.util.Arrays;

public class Quiz1 {
	public static void main(String[] args) {
		double[] numbers = new double[10];
		System.out.println(Arrays.toString(numbers));
		long[] largeNumbers = new long[5];
		System.out.println(Arrays.toString(largeNumbers));

		char[] word = {'h','e','l','p'};
		System.out.println(Arrays.toString(word));

		String[] sentence = {"It", "is", "cold"};
		System.out.println(Arrays.toString(sentence));

		int[] myNumbers = {1, 2, 3, 4, 6};
		System.out.println(Arrays.toString(sentence));

		for(int count=0; count < sentence.length;count++){
			System.out.printf("Element %d = %s%n",count,sentence[count]);
		}

		printInReverse(sentence);
		String[] toPrint = {"Another","array"};
		printInReverse(toPrint);

		System.out.println("Going through array with for each loop:");
		for(String thing: sentence){
			System.out.printf("Element = %s%n",thing);
		}
	}

	private static void printInReverse(String[] strings) {
		System.out.printf("\nPrinting array with length %d in reverse%n",strings.length);
		for(int count =  strings.length-1; count >= 0; count--){
			System.out.printf("Element %d = %s%n",count, strings[count]);
		}


	}
}