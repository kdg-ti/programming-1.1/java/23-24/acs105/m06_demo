import java.util.Arrays;
import java.util.Random;

public class Lottery {
	public static void main(String[] args) {

		int[][] lotteryNumbers = new int[4][6];
		Random random = new Random();

		for (int week = 0; week<4;week++) {
			for (int count = 0; count < lotteryNumbers[week].length; count++) {
				lotteryNumbers[week][count]= random.nextInt(42)+1;
			}
		}
		for(int[] week : lotteryNumbers)
		System.out.println(Arrays.toString(week));
	}
}
